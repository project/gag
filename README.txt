
*******************
Installation
*******************
Typical module install:
- Make sure dependancies are installed.
- Extract GAG tar-ball into /sites/all/modules/
- Place the follwoing in the /sites/all/libraries/ folder:
	-/customdata/jquery.customdata.js from https://github.com/ubilabs/jquery-customdata
	-/galleria extract the galleria.zip from https://github.com/aino/galleria/tree/master/src/
- visit /admin/build/modules and turn on the GAG module

******************
Configuration
******************

-Visit /admin/settings/gallery_assist/extras.
-Turn on the Galleria Viewing option and set your height and width (height cannot be 'auto', Must be at least 60)