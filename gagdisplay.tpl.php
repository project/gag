<?php
?>

<div data-themepath="<?php print $galleria_theme_path; ?>" data-height="<?php print $gag_height; ?>" data-width="<?php print $gag_width; ?>" class="galleria" style="visibility:hidden">
  <?php if(count($items) > 0): ?>
  <?php
    foreach ($items as $index => $item){
      $title = $item->ptitle;
      $tpath = $base_path.$item->tpath;
      $ppath = $base_path.$item->ppath;
      $opath = $base_path.$item ->opath;
  ?>
    <img class="preview" alt="<?php print $title; ?>" src="<?php print $ppath; ?>"/>
  <?php
    }
  ?>
  <?php endif; ?>
</div>