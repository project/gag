// drupal_add_js(drupal_get_path('module', 'gag').'/js/jquery.gag.js', 'module', 'footer');
if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    function F() {}
    F.prototype = o;
    return new F();
  };
}

var drupalGalleria = {
  $elem: false,
  galleryiaAPI: false,
  options: {
  },
  settings: {
    height: 500,
    width: 'auto',
    themepath: false,
  },
  getData: function(){
    var gData = new Array();
    $.extend(this.settings, this.$elem.customdata());
    this.settings.height = this.checkHeightWidthAuto(this.settings.height);
    this.settings.width = this.checkHeightWidthAuto(this.settings.width);
  },
  checkHeightWidthAuto: function(val){
    if(val == 'auto')
    return val;
    return val + 'px';
  },
  init: function(options, elem){
    $.extend(
      this.options, 
      options
    );
    this.$elem = $(elem);

    this.getData();
    this.$elem.css({
      display: 'block',
      position: 'relative',
      height: this.settings.height,
      width: this.settings.width
    });
    Galleria.loadTheme(this.settings.themepath);
    this.$elem.galleria(this.settings);
  }
};

(function($){
  $.drupalGalleria = $.fn.extend({
    drupalGalleria: function(options){
      return $(this).each(function(){
        var galleriaInstance = Object.create(drupalGalleria);
        galleriaInstance.init(options, this);
        $(this).css('visibility', 'visible');
        $(this).data('drupalGalleriaInstance', galleriaInstance);
      });
    }
  });
})(jQuery);

$(document).ready(function(){
  $('div.galleria').drupalGalleria();
});